%%% Copyright (C) 2012-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Méthodes numériques en actuariat avec R»
%%% https://gitlab.com/vigou3/methodes-numeriques-en-actuariat-avec-r
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter[Planification d'une simulation dans R]{%
  Planification d'une simulation \\ dans R}
\label{chap:planification} %|

\def\scriptfilename{\currfilebase.R}
\SweaveOpts{echo=FALSE, prefix=FALSE}
\SweaveInput{share/license-by-sa.nw}

%% Le traitement de ce fichier avec Sweave nécessite le moteur
%% 'RweaveExtraLatex' du paquetage RweaveExtra.
<<eval=TRUE, ignore.on.tangle=TRUE>>=
if (!"RweaveExtra" %in% .packages())
    stop("paquetage RweaveExtra non chargé")
if (packageVersion("RweaveExtra") < "1.2.0")
    stop("paquetage RweaveExtra trop ancien; version >= 1.2.0 requise")
@

%% Extraction des exemples avec Stangle.
<<echo=FALSE, results=hide, ignore.on.tangle=TRUE>>=
source("Stangle.R")        # lancement de Stangle
options(width = 55)
@
<<planification>>=
<<license-by-sa>>
@

Il existe de multiples façons de réaliser une simulation dans R, mais
certaines sont plus efficaces et simples à administrer que d'autres.
Cette annexe compare deux approches à l'aide d'un exemple simple de
nature statistique.

\section{Contexte}
\label{sec:planification:contexte}

Soit $X_1, \dots, X_n$ un échantillon aléatoire tiré d'une population
distribuée selon une loi uniforme sur l'intervalle
$(\theta - \frac{1}{2}, \theta + \frac{1}{2})$ et trois estimateurs
sans biais du paramètre inconnu $\theta$:
\begin{enumerate}
\item la moyenne arithmétique
  \begin{displaymath}
    \hat{\theta}_1 = \frac{1}{n} \sum_{i = 1}^n X_i\,;
  \end{displaymath}
\item la médiane empirique
  \begin{displaymath}
    \hat{\theta}_2 =
    \begin{cases}
      X_{(\frac{n+1}{2})},
      & \text{$n$ impair} \\
      \frac{1}{2}(X_{(\frac{n}{2})} + X_{(\frac{n}{2} + 1)}),
      & \text{$n$ pair},
    \end{cases}
  \end{displaymath}
  où $X_{(k)}$ est la $k${\ieme} statistique d'ordre de l'échantillon
  aléatoire;
\item la mi-étendue
  \begin{displaymath}
    \hat{\theta}_3 = \frac{X_{(1)} + X_{(n)}}{2}.
  \end{displaymath}
\end{enumerate}

À l'aide de la simulation, nous voulons, d'une part, vérifier si les
trois estimateurs sont bel et bien sans biais et, d'autre part,
déterminer lequel a la plus faible variance.

Pour ce faire, nous devons d'abord simuler un grand nombre $N$
d'échantillons aléatoires de taille $n$ d'une distribution
$U(\theta - \frac{1}{2}, \theta + \frac{1}{2})$ pour une valeur de
$\theta$ choisie. Pour chaque échantillon, nous calculerons ensuite
les trois estimateurs ci-dessus, puis leurs moyennes et leurs
variances. Si la moyenne des $N$ estimateurs $\hat{\theta}_i$,
$i = 1, 2, 3$ est près de $\theta$, alors nous pourrons conclure que
$\hat{\theta}_i$ est sans biais. De même, nous déterminerons lequel
des trois estimateurs a la plus faible variance selon le classement
des variances empiriques.

Tout le code informatique présenté dans cette annexe est livré avec
l'ouvrage et reproduit à la \autoref{sec:planification:code}.

\section{Approche monolithique}
\label{sec:planification:monolithique}

L'approche la plus intuitive pour mettre en œuvre une étude de
simulation avec R consiste à construire un script qui effectue tout le
travail de manière monolithique: simulation des échantillons, calcul
des estimateurs, calcul des statistiques. Au cœur du script se trouve
généralement une boucle \code{for}. Cette approche requiert
d'initialiser une matrice de $3$ lignes et $N$ colonnes (ou l'inverse)
pour stocker les valeurs des trois estimateurs pour chaque simulation.
Une fois la matrice remplie dans la boucle, il ne reste plus qu'à
calculer la moyenne et la variance par ligne pour obtenir les
résultats souhaités.

\gotorbox{Vous trouverez un exemple de script de simulation aux lignes
  \reflines{planification:script} du code informatique. Prenez le
  temps d'examiner ce script et de lire les commentaires.}

<<planification, results=hide>>=
###
### APPROCHE MONOLITHIQUE  `\labelline{planification:script}`
###

## Bonne habitude à prendre: stocker les paramètres dans des variables
## faciles à modifier au lieu de les écrire explicitement dans le
## code.
size <- 100                # taille de chaque échantillon
nsimul <- 10000            # nombre de simulations
theta <- 0                 # vraie valeur du paramètre

## Les lignes ci-dessous éviteront de faire deux additions
## 'nsimul' fois.
a <- theta - 0.5           # borne inférieure de l'uniforme
b <- theta + 0.5           # borne supérieure de l'uniforme

## Initialisation de la matrice dans laquelle seront stockées les
## valeurs des estimateurs. Les noms donnés aux lignes de la matrice
## permettent de facilement identifier les estimateurs.
x <- matrix(0, nrow = 3, ncol = nsimul)
rownames(x) <- c("mean", "median", "midrange")

## Simulation comme telle.
for (i in seq_len(nsimul))
{
    u <- runif(size, a, b)
    x[, i] <- c(mean(u),        # moyenne
                median(u),      # médiane
                mean(range(u))) # mi-étendue
}

## Nous pouvons maintenant calculer la moyenne et la variance par
## ligne.
rowMeans(x) - theta        # vérification du biais
apply(x, 1, var)           # comparaison des variances  `\labelline{planification:script:fin}`
@

Il n'est pas rare de vouloir reprendre une simulation avec différents
paramètres. Pour faciliter l'exécution répétée d'une simulation, il
suffit de tout placer à l'intérieur d'une fonction qui prend en
arguments les principaux paramètres de la simulation. Dans le code
informatique, c'est le rôle de la fonction \code{simul1} définie aux
lignes \reflines{planification:simul1}.

<<planification, results=hide>>=
## Réorganisation du script sous forme de fonction pour en faciliter
## l'appel répété. La fonction ci-dessous reprend le code du script
## sans les commentaires.
simul1 <- function(n, size, theta)  #-*- `\labelline{planification:simul1}`
{
    a <- theta - 0.5
    b <- theta + 0.5

    x <- matrix(0, nrow = 3, ncol = n)
    rownames(x) <- c("mean", "median", "midrange")

    for (i in seq_len(n))
    {
        u <- runif(size, a, b)
        x[, i] <- c(mean(u), median(u), mean(range(u)))
    }

    list(bias = rowMeans(x) - theta,
         variance = apply(x, 1, var))
}                          #-*- `\labelline{planification:simul1:fin}`
@

Pour réaliser une simulation, il suffit d'appeler la fonction avec les
paramètres choisis.
<<echo=TRUE, ignore.on.tangle=TRUE>>=
simul1(10000, 100, 0)
@


\section{Approche modulaire}
\label{sec:planification:modulaire}

La fonction \code{simul1} amalgame trois concepts différents: la
simulation d'un échantillon, le calcul des estimateurs et le calcul
des statistiques. Cela est déjà contraire à la %
\link{https://fr.wikipedia.org/wiki/Philosophie_d\%27Unix}{philosophie
  Unix} %
qui dicte que chaque fonction devrait ne faire qu'une seule chose, et
la faire bien. Plus prosaïquement, l'aspect monolithique de la
fonction rend plus difficile son débogage: si quelque chose ne se
passe pas bien, quelle étape fait défaut?

Une meilleure approche pour la simulation dans R consiste à travailler
de manière modulaire: une première fonction est d'abord responsable
d'effectuer les calculs pour une seule simulation. Ensuite, une
seconde fonction se charge de répéter ces calculs un grand nombre de
fois et de produire les statistiques. En lieu et place d'une boucle,
cette dernière pourra faire appel à la fonction \code{replicate} dont
le rôle consiste justement à répéter une expression un certain nombre
de fois à l'identique. Le stockage des résultats dans une matrice est
alors automatiquement pris en charge.

\gotorbox{Étudiez les définitions des deux fonctions aux lignes
  \reflines{planification:modulaire} du code informatique.}

<<planification, results=hide>>=
###
### APPROCHE MODULAIRE  `\labelline{planification:modulaire}`
###

## Dans l'approche modulaire, nous créons d'abord, une fonction
## chargée des calculs pour une seule simulation. Dans notre exemple,
## il s'agit de calculer les trois estimateurs pour un échantillon.
## Comme la simulation de l'échantillon en tant que tel est très
## simple, il est tout à fait raisonnable d'intégrer cette étape à la
## fonction. (Non mais, quel est ce nom de fonction? Eh bien: «r»
## comme le préfixe de toutes les fonctions de simulation de R, puis
## «mmm» pour «moyenne, médiane, mi-étendue».)
rmmm <- function(size, a, b)
{
    u <- runif(size, a, b)
    c(mean = mean(u),
      median = median(u),
      midrange = mean(range(u)))
}

## La seconde fonction se charge de gérer les paramètres de la
## simulation et d'appeler la fonction 'rmmm' à répétition à l'aide de
## 'replicate'.
simul2 <- function(n, size, theta)
{
    a <- theta - 0.5
    b <- theta + 0.5

    x <- replicate(n, rmmm(size, a, b))

    list(bias = rowMeans(x) - theta,
         variance = apply(x, 1, var))
}                          #-*- `\labelline{planification:modulaire:fin}`
@

Comme précédemment, un appel de la seconde fonction permet de
compléter une simulation.
<<echo=TRUE, ignore.on.tangle=TRUE>>=
simul2(10000, 100, 0)
@


\section{Gestion des fichiers}
\label{sec:planification:fichiers}

Pour un petit projet comme celui utilisé en exemple ici, il est simple
et pratique de placer tout le code informatique dans un seul fichier
de script. Pour un plus gros projet, cependant, il vaut souvent mieux
avoir recours à plusieurs fichiers différents. J'utilise pour ma part
un fichier par fonction. Selon l'approche modulaire, cela signifie que
les fonctions \code{rmmm} et \code{simul2} se trouvent dans des
fichiers \code{rmmm.R} et \code{simul2.R}.

Ensuite un autre fichier de script, disons \code{go.R} se charge de
définir les fonctions avec \code{source}, puis de lancer la
simulation. Dans notre exemple, le fichier \code{go.R} contiendrait
les lignes suivantes:
\begin{Schunk}
\begin{Verbatim}
source("rmmm.R")
source("simul.R")
simul2(10000, 100, 0)
\end{Verbatim}
\end{Schunk}

Pour démarrer une simulation, il suffit alors d'évaluer ce fichier
avec \code{source}.
<<echo=TRUE, eval=FALSE, ignore.on.tangle=TRUE>>=
source("go.R")
@


\section{Exécution en lot}
\label{sec:planification:batch}

L'organisation des fichiers exposée à la section précédente simplifie
l'exécution de simulations en lot (\emph{batch}) pour en accélérer le
traitement. Dans ce mode d'exécution de R, aucune interface graphique
n'est démarrée et tous les résultats sont redirigés vers un fichier
pour consultation ultérieure. Pour les simulations demandant un long
temps de calcul, c'est très pratique.

On exécute R en lot à la ligne de commande avec \code{R CMD BATCH}. La
commande prend en argument un fichier de script. Dans notre exemple,
il s'agirait du fichier \code{go.R} responsable de lancer une
simulation complète. En supposant que le répertoire courant est celui
contenant les fichiers de script, la commande suivante réalise une
simulation complète en lot.
\begin{Schunk}
\begin{Verbatim}
$ R CMD BATCH go.R
\end{Verbatim}
\end{Schunk}

La sortie de la commande (et donc tous les résultats des expressions
du fichier \code{go.R}) est placée par défaut dans le fichier
\code{go.Rout}.


\section{Conclusion}
\label{sec:planification:conclusion}

Le nombre de simulations, $N$, et la taille de l'échantillon, $n$, ont
tous deux un impact sur la qualité des résultats, mais de manière
différente. Quand $n$ augmente, la précision des estimateurs augmente.
Ainsi, dans l'exemple ci-dessus, le biais et la variance des
estimateurs de $\theta$ seront plus faibles. D'autre part,
l'augmentation du nombre de simulations diminue l'impact des
échantillons aléatoires individuels et, de ce fait, améliore la
fiabilité des conclusions de l'étude.

D'ailleurs, les conclusions de l'étude de simulation sur le biais et
la variance des trois estimateurs de la moyenne d'une loi uniforme
sont les suivantes: les trois estimateurs sont sans biais et la
mi-étendue a la plus faible variance. En effet, on peut démontrer
mathématiquement que, pour $n$ impair,
\begin{align*}
  \var{\hat{\theta}_1} &= \frac{1}{12 n} \\
  \var{\hat{\theta}_2} &= \frac{1}{4n + 2} \\
  \var{\hat{\theta}_3} &= \frac{1}{2(n + 1)(n + 2)}
\end{align*}
et donc
\begin{displaymath}
  \var{\hat{\theta}_3} \leq \var{\hat{\theta}_1} \leq \var{\hat{\theta}_2}
\end{displaymath}
pour tout $n \geq 2$.


\section{Code informatique}
\label{sec:planification:code}

\scriptfile{\scriptfilename}
\lstinputlisting[firstline=\scriptfirstline]{\scriptfilename}

%%% Local Variables:
%%% TeX-master: "methodes-numeriques-en-actuariat-avec-r"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
